<form class="login" method="post" autocomplete="false">
    <div style="text-align: center">
        <img alt="toa" src="<?php echo site_url('images/logo.png'); ?>">
    </div>
    <div class="form-group">
        <label class="input-group input-add-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control" name="email_address" value="" placeholder="Email">
        </label>
    </div>
    <div class="form-group">
        <label class="input-group input-add-group">
            <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
            <input type="password" class="form-control" name="user_password" value="" placeholder="Password">
        </label>
    </div>
    <div class="checkbox">
        <div class="custom-input remember_me">
            <input type="checkbox" name="remember_me" value="1" id="remember"><label for="remember">Remember me</label>
        </div> 
    </div>
    <div class="form-group">
        <button class="btn btn-default btn-block"><i class="fa fa-sign-in" aria-hidden="true"></i> LOGIN</button>
    </div>
    <div class="output"></div>
</form>

</form>
<div class="backto-home text-center">
    <a href="<?php echo site_url('') ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
</div>