<!DOCTYPE html>
<html>
    <head>
        <meta name="page-url" class="page-url" content="<?php echo site_url(); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php echo isset($title) ? $title : 'Default Title' ; ?></title>
        
        <link href="<?php echo site_url('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('css/bootstrap-theme.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('css/theme.css'); ?>" rel="stylesheet" type="text/css"/>
        
        <link href="<?php echo site_url('jquery-ui/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('jquery-ui/jquery-ui.structure.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('jquery-ui/jquery-ui.theme.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('css/font.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('css/datatables.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('css/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css"/>

        
        <link href="<?php echo site_url('css/fullcalendar.min.css'); ?>" rel="stylesheet" type="text/css"/>

        <link href="<?php echo site_url('css/jquery.qtip.min.css'); ?>" rel="stylesheet" type="text/css"/>
       
        <link href="<?php echo site_url('css/style.css'); ?>" rel="stylesheet" type="text/css"/>

        
        
        
        <script src='<?php echo site_url('js/moment.min.js'); ?>' type="text/javascript"></script>
        <script src="<?php echo site_url('js/jquery-2.1.3.min.js'); ?>"></script>

        <script src="<?php echo site_url('jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?php echo site_url('js/jquery.form.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('js/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo site_url('js/notify.js'); ?>"></script>
        <script src="<?php echo site_url('js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('js/cookies.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>

        <script src='<?php echo site_url('js/fullcalendar.min.js'); ?>' type="text/javascript"></script>
        <script src="<?php echo site_url('js/jquery.qtip.min.js'); ?>" type="text/javascript"></script>
        
        
        <script src="<?php echo site_url('js/script.js'); ?>" type="text/javascript"></script>
        
        
    </head>
    <body>
<div class="main<?php echo (empty(get_cookie('sidemenu'))?'':' close-sidemenu') ?>">