<div class="side-menu">
    <div class="side-menu-content">
        <ul>
            <li>
                <a href="<?php echo site_url(); ?>">
                    <i class="fa fa-desktop" aria-hidden="true"></i> <span>NEW ITEM</span>
                </a>
            </li>
            <li>
                <a href="<?php echo site_url(); ?>">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <span>ACCOUNTABILITY FORM</span>                                  
                </a>
            </li>
            <li>
                <a href="<?php echo site_url(); ?>">
                    <i class="fa  fa-bar-chart" aria-hidden="true"></i> <span>REPORT</span>
                </a>
            </li>
            <li>
                <a href="<?php echo site_url(); ?>">
                    <i class="fa fa-cog" aria-hidden="true"></i> <span>SETTINGS</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="side-panel-button fa fa-bars"></div>
</div>  