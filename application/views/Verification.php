<form class="login" method="post" autocomplete="false">
    <div style="text-align: center">
        <img alt="toa" src="<?php echo site_url('images/logo.png'); ?>">
    </div>

    <div class="form-group">
        <label class="input-group input-add-group">
            <span class="input-group-addon"><i class="fa fa-user-plus" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="email_address" value="" placeholder="Enter the verification code">
        </label>
    </div>

    <div class="form-group">
        <button class="btn btn-default btn-block"><i class="fa fa-id-card-o" aria-hidden="true"></i> SUBMIT</button>
    </div>
    <div class="output"></div>
</form>
</form>
<div class="backto-home text-center">
    <a href="<?php echo site_url('') ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
</div>