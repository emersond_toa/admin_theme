<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="pull-right">
  <!--         <a href class="btn btn-warning navbar-btn logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>-->
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div id='calendar' class="calendar"></div>
</div>


<!-- Modal -->
<div class="modal fade" id="show-modal-calendar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Reservation</h4>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-6 text-center">
                        <a href="<?php echo site_url('home/login') ?>" class="btn btn- btn-block btn-primary">LOGIN</a> 
                    </div>
                    <div class="col-md-6  text-center">
                        <a href="<?php echo site_url('home/register') ?>" class="btn btn-default btn-block  btn-primary">REGISTER</a> 
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>Select Time</h3>
                    </div>
                    <div class="col-md-6 text-center">
                       
                        <div class="list-group">
                            <a href="#" class="list-group-item">7:00 AM TO 8:00 AM</a>
                            <a href="#" class="list-group-item">8:00 AM TO 9:00 AM</a>
                            <a href="#" class="list-group-item">10:00 AM TO 11:00 AM</a>
                            <a href="#" class="list-group-item">11:00 AM TO 12:00 NN</a>
                        </div>
                    </div>
                    <div class="col-md-6  text-center">
                        <ul class="list-group">
                            <a href="#" class="list-group-item">1:00 AM TO 2:00 AM</a>
                            <a href="#" class="list-group-item">2:00 AM TO 3:00 AM</a>
                            <a href="#" class="list-group-item">4:00 AM TO 5:00 AM</a>
                            <a href="#" class="list-group-item">6:00 AM TO 7:00 NN</a>
                        </ul>
                    </div>
                </div>
                
                                    <div class="col-md-12 text-center">
                        <h3>Select Seat Number</h3>
                    </div>
                <table class="table table-bordered select-seat">
                    <?php
                    for ($row = 1; $row <= 4; $row++) {
                        echo "<tr> \n";
                        for ($col = 1; $col <= 10; $col++) {
                            $p = $col * $row;
                            echo "<td data-number='$p'>$p</td> \n";
                        }
                        echo "</tr>";
                    }
                    ?>  
                </table>   
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>
</div>