<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
    }
    function index() {
        $this->load->view('includes/header');
        if (($this->session->userdata('user_session'))) {
            $this->load->view('includes/header-nav');
            $this->load->view('includes/sidemenu');
            $this->load->view('main');
        } else {
            $this->load->view('calendar');
        }
        $this->load->view('includes/footer');
    }
    function login() {
        $this->load->view('includes/header');
        $this->load->view('login');
        $this->load->view('includes/footer');
    }
    
    function register() {
        $this->load->view('includes/header');
        $this->load->view('register');
        $this->load->view('includes/footer');
    }
    
    function verification() {
        $this->load->view('includes/header');
        $this->load->view('verification');
        $this->load->view('includes/footer');
    }
    
    

}
