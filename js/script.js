var EM = EM || {};
EM.script = (function (j) {
    var _url = j('.page-url').attr('content');
    var init = function () {
        togglemenu();
        login();
        page_progress();
        default_datepicker();
        
        initCalendar();
    };
    var page_progress = function () {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
    };
    var default_datepicker = function(){
        j('.default-datepicker').datepicker();
    };
    var login = function () {
        j('.login').validate({
            rules: {
                email_address: {
                    required: true,
                    email: true
                },
                user_password: 'required'
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('error-input');
                    _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                    var len = _elm.parent().find('.form-control-feedback').length;
                    if (len === 0) {
                        _elm.parent().append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
                    }
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('error-input');
                    _elm.parent().popover('destroy');
                    var len = _elm.parent().find('.form-control-feedback').length;
                    if (len > 0) {
                        _elm.parent().find('.form-control-feedback').remove();
                    }
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'request/login',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            _this.find('.output').html('<div class="alert alert-success text-center">'+e.text+'</div>');
                            window.location.reload();
                        } else if (e.message === 'error') {
                            _this.find('.output').html('<div class="alert alert-warning text-center">'+e.text+'</div>');
                        }
                    }
                });
                return false;
            }
        });
    };
    var togglemenu = function () {
        var _main = j('.main');
        _main.find('.side-panel-button').click(function () {
            if (_main.hasClass('close-sidemenu') === false) {
                _main.addClass('hide-span');
                _main.find('.content').animate({'margin-left': '60px'}, 200, function () {
                    _main.find('.content').removeAttr('style');
                });
                _main.find('.side-menu').animate({width: '60px'}, 200, function () {
                    _main.addClass('close-sidemenu');
                    j.cookie("sidemenu", 'sidemenu');
                    _main.find('.side-menu').removeAttr('style');
                    _main.find('.side-menu').find('span').each(function () {
                        var _this = j(this);
                        var _badge = _this.parent().find('.badge');
                        var _name = _this.html();
                        if (_badge.length > 0) {
                            _name += (" <i class='badge'>" + _badge.html() + "</i>");
                        }
                        _this.parent().popover({content: _name, container: 'body', placement: 'right', trigger: 'hover', html: true});
                    });
                });
            } else {
                _main.find('.content').animate({'margin-left': '300px'}, 200, function () {
                    _main.find('.content').removeAttr('style');
                });
                _main.find('.side-menu').animate({width: '300px'}, 200, function () {
                    _main.removeClass('close-sidemenu');
                    _main.find('.side-menu').removeAttr('style');
                    _main.removeClass('hide-span');
                    _main.find('.side-menu').find('span').each(function () {
                        var _this = j(this);
                        _this.parent().remove('data-content').popover('destroy');
                    });
                    j.removeCookie("sidemenu");
                    j.cookie("sidemenu", '', {expires: -1, path: '/'});
                });
            }
        });
        j(window).on('resize load', function () {
            var _this = j(this);
            if (_this.width() < 768) {
                _main.addClass('close-sidemenu');
                _main.find('.side-menu').find('span').each(function () {
                    var _this = j(this);
                    var _badge = _this.parent().find('.badge');
                    var _name = _this.html();
                    if (_badge.length > 0) {
                        _name += (" <i class='badge'>" + _badge.html() + "</i>");
                    }
                    _this.parent().popover({content: _name, container: 'body', placement: 'right', trigger: 'hover', html: true});
                });
            }
        });
    };
    
    
    
    
    
    var tConvert = function (time) {
        if (typeof time !== 'undefined') {
            time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
            if (time.length > 1) {
                time = time.slice(1);
                time[5] = +time[0] < 12 ? 'am' : 'pm';
                time[0] = +time[0] % 12 || 12;
            }
            return time.join(''); // return adjusted time or original string
        } else {
            return time;
        }
    };
    var initCalendar = function (today) {
        var _modal = j('#show-modal-calendar');
        j('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: today,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: {
                url: 'request/calendar',
                error: function () {
                    j('#script-warning').show();
                }
            },
            loading: function (bool) {
                j('#loading').toggle(bool);
            },
            eventRender: function (event, element) {
//                var _s = tConvert((event.start._i).split('T')[1]);
//                var _e = tConvert((event.end._i).split('T')[1]);
//                var ttip = '';
//                var _des = event.description == '' ? 'None' : event.description;
//                ttip += '<div style="font-size:12px; font-weight:normal;">';
//                ttip += '<h5 class="text-info" style="font-weight:bold;">'+event.title+'</h5>';
//                ttip += '<div style="padding:2px;"><span class="text-warning" style="font-weight:bold;">Date:</span> '+(event._start._i).split('T')[0]+' to '+(event._end._i).split('T')[0]+'</div>';
//                
//                if(typeof _s !== 'undefined' && _e !== 'undefined'){
//                    ttip += '<div style="padding:2px;"><span class="text-warning" style="font-weight:bold;">Time:</span> '+_s+' - '+_e+'</div>';
//                    element.find('.fc-time').html(_s+'-'+_e);
//                }
//                
//                ttip += '<div style="padding:2px;"><span class="text-warning" style="font-weight:bold;">Who:</span> '+event.who+'</div>';
//                ttip += '<div style="padding:2px;"><span class="text-warning" style="font-weight:bold;">Where:</span> '+event.where+'</div>';
//                ttip += '<div style="padding:2px;"><div class="text-info" style="font-weight:bold;">Description</div> <div style="padding-top:5px;">'+_des+'</div></div>';
//                
//                ttip += '</div>';
//                
//                element.qtip({
//                    position: {
//                        my: 'bottom center',
//                        at: 'bottom center',
//                        target: 'mouse'
//                    },
//                    content: ttip,
//                    style: {
//                        classes: 'qtip-dark qtip-rounded qtip-shadow'
//                    }
//                });
            },
            
            eventDrop: function (event, delta, revertFunc) {
                var _s = tConvert((event.start._i).split('T')[1]);
                var _e = tConvert((event.end._i).split('T')[1]);
                var _sd = (event._start._i).split('T')[0];
                var _ed = (event._end._i).split('T')[0];
            },
            dayClick: function (date, jsEvent, view) {
                var d = new Date(date._d);
                var dmonth = d.getMonth() + 1;
                var _month = dmonth < 10 ? '0' + dmonth : dmonth;
                var _day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
                console.log(d.getFullYear() + '-' + _month + '-' + _day);
                
                
                _modal.modal({
                    backdrop: 'static',
                    keyboard: false
                }); 
                
            }
        });
    };
    
    
    
    
    
    
    
    
    
    
    return{
        init: init
    };
})(jQuery);

